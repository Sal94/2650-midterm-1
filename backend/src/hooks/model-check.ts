// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { data } = context;
    
    if ( data['year'] < 1885 || data['year'] > 2020 ) {
      throw new Error( "BAD YEAR: " + data['year']);
    } else if ( data['mileage'] <= 0 ) {
      throw new Error( "BAD MILEAGE: " + data['mileage']);
    }
    else {
      console.log('truncating make/model to 30 chars')
      context.data['make'] =  data['make'].substring(0,30)
      context.data['model'] =  data['model'].substring(0,30)
      console.log( "GOOD: " + context.data['make']);
      console.log( "GOOD: " + context.data['model']);
      console.log( "GOOD: " + data['year']);
      console.log( "GOOD: " + data['mileage']);
      return context;
    }
  };
}
